﻿using System;
using System.Collections.Generic;
using System.Linq;
 
public class Program
{
    public static void Main()
    {
        string requirements = 

@"//////////////////////////////////////////
Program Requirements:
Using LINQ (Language Integrated Query)
Author: Nick Richard

*After* completing the required tutorial, create the following program:

 1) Prompt the user for last name. Return full name, occupation, and age.
 
 2) Prompt user for age and occupation (Dev or Manager). Return full name.
     (*Must* include data validation on numeric data.)

 3) Allow user to press any key to return back to command line.
 
  *Notes:*
  - LINQ uses programming language syntax to query data.
  - LINQ uses SQL-like syntax to produce usable objects.
  - LINQ can be used to quer many different types of data,
    includeing relational, XML, and even objects.
    
//////////////////////////////////////////";

        Console.WriteLine(requirements);

        Console.WriteLine("\n\nNow: " + DateTime.Now.ToString("ddd, M/d/yy H:mm:ss t"));

        var people = GenerateListOfPeople();
 
        Console.WriteLine("\n***Finding Items in Collections***");
        Console.WriteLine("\nWhere:");
        //There will be two Persons in this variable: the "Steve" Person and the "Jane" Person
        var peopleOverTheAgeOf30 = people.Where(x => x.Age > 30);
        foreach (var person in peopleOverTheAgeOf30)
        {
            Console.WriteLine(person.FirstName);
        }
 
        Console.WriteLine("\nSkip:");
        IEnumerable<Person> afterTwo = people.Skip(2);
        foreach (var person in afterTwo)
        {
            Console.WriteLine(person.FirstName);
        }
 
        Console.WriteLine("\nTake:");
        IEnumerable<Person> takeTwo = people.Take(2);
        foreach (var person in takeTwo)
        {
            Console.WriteLine(person.FirstName);
        }
 
 
        Console.WriteLine("\n***Changing Each Item in Collections***");
 
        Console.WriteLine("\nSelect:");
        IEnumerable<string> allFirstNames = people.Select(x => x.FirstName);
        foreach (var firstName in allFirstNames)
        {
            Console.WriteLine(firstName);
        }
 
 
        Console.WriteLine("\nFullname class and objects:");
        IEnumerable<FullName> allFullNames = people.Select(x => new FullName { First = x.FirstName, Last = x.LastName });
        foreach (var fullName in allFullNames)
        {
            Console.WriteLine($"{fullName.Last}, {fullName.First}");
        }
 
        Console.ReadKey();
 
        Console.WriteLine("\n***Finding One Item in Collections***");
 
//first or default
        Console.WriteLine("\nFirstOrDefault:");
        Person firstOrDefault = people.FirstOrDefault();
        Console.WriteLine(firstOrDefault.FirstName);
 
//as filter
        Console.WriteLine("\nFirstOrDefault as filter:");
        var firstThirtyYearOld1 = people.FirstOrDefault(x => x.Age == 30);
        var firstThirtyYearOld2 = people.Where(x => x.Age == 30).FirstOrDefault();
        Console.WriteLine(firstThirtyYearOld1.FirstName); //Will output "Brendan"
        Console.WriteLine(firstThirtyYearOld2.FirstName); //Will also output "Brendan"
 
//how or default works
        Console.WriteLine("\nHow Or Default works:");
        List<Person> emptyList = new List<Person>();
        Person willBeNull = emptyList.FirstOrDefault();
 
        Person willAlsoBeNull = people.FirstOrDefault(x => x.FirstName == "John");
 
        Console.WriteLine(willBeNull == null); // true
        Console.WriteLine(willAlsoBeNull == null); //true
 
//last or default
        Console.WriteLine("\nLastOrDefault as filter:");
        Person lastOrDefault = people.LastOrDefault();
        Console.WriteLine(lastOrDefault.FirstName);
        Person lastThirtyYearOld = people.LastOrDefault(x => x.Age == 30);
        Console.WriteLine(lastThirtyYearOld.FirstName);
 
//singleordefault as filter
        Console.WriteLine("\nSingleOrDefault:");
        Person single = people.SingleOrDefault(x => x.FirstName == "Eric");
        Console.WriteLine(single.FirstName);
        // Uncomment the next line to see it throw an exception
        // Person singleDev = people.SingleOrDefault(x => x.Occupation == "Dev");
 
//FINDING DATA ABOUT COLLECTIONS
        Console.WriteLine("\n***Finding Data About Collections***");
 
//count1
        Console.WriteLine("\nCount():");
        int numberOfPeopleInList = people.Count();
        Console.WriteLine(numberOfPeopleInList);
 
//count2
        Console.WriteLine("\nCount() with predicate expression:");
        int peopleOverTwentyFive = people.Count(x => x.Age > 25);
        Console.WriteLine(peopleOverTwentyFive);
 
//Any1
        Console.WriteLine("\nAny():");
        bool thereArePeople = people.Any();
        Console.WriteLine(thereArePeople);
        bool thereAreNoPeople = emptyList.Any();
        Console.WriteLine(thereAreNoPeople);
 
//All
        Console.WriteLine("\nAll():");
        bool allDevs = people.All(x => x.Occupation == "Dev");
        Console.WriteLine(allDevs);
        bool everyoneAtLeastTwentyFour = people.All(x => x.Age >= 24);
        Console.WriteLine(everyoneAtLeastTwentyFour);
 

        Console.WriteLine("***COnverting Results to Collections***");
        Console.WriteLine();

//ToList
        Console.WriteLine("\nToList:");
        List<Person> listOfDevs = people.Where(x => x.Occupation == "Dev").ToList();
        foreach (var person in listOfDevs)
        {
                Console.WriteLine($"{person.FirstName}");
        }
        Console.WriteLine();

//ToArray
        Console.WriteLine("\nToArray:");
        Person[] arrayOfDevs = people.Where(x => x.Occupation == "Dev").ToArray();
        foreach(var person in arrayOfDevs)
        {
                Console.WriteLine($"{person.FirstName}");
        }
        Console.WriteLine();

        Console.WriteLine("***Required Program***");
        Console.WriteLine();

        Console.WriteLine("Note: Searches are case sensitive.");
        Console.WriteLine();

        Console.Write("Please enter last name: ");
        string lname = Console.ReadLine();
        var lnameIsX = people.Where(x => x.LastName == lname);
        foreach (var person in lnameIsX)
        {
                Console.WriteLine("Matching Criteria: " + person.FirstName + " " + person.LastName + " is a " + person.Occupation + ", and is " + person.Age + " yrs old.");
        }

        Console.Write("\nPlease enter age: ");
        int age=0;

        while (!int.TryParse(Console.ReadLine(), out age))
        {
                Console.WriteLine("\nAge must be an integer.");
                Console.WriteLine("Please enter an age: ");
        }

        Console.Write("\nPlease enter an occupation: ");
        string occupation = Console.ReadLine();

        var ageOccupation = people.Where(x => x.Occupation == occupation && x.Age == age);
        foreach (var person in ageOccupation)
        {
                Console.WriteLine("Matching criteria: " + person.FirstName + " " + person.LastName);
        }
        Console.WriteLine();

        Console.WriteLine("Press any key to exit!");
        Console.ReadKey();
    }
 
    public static List<Person> GenerateListOfPeople()
    {
        var people = new List<Person>();
 
        people.Add(new Person { FirstName = "Eric", LastName = "Fleming", Occupation = "Dev", Age = 24 });
        people.Add(new Person { FirstName = "Steve", LastName = "Smith", Occupation = "Manager", Age = 40 });
        people.Add(new Person { FirstName = "Brendan", LastName = "Enrick", Occupation = "Dev", Age = 30 });
        people.Add(new Person { FirstName = "Jane", LastName = "Doe", Occupation = "Dev", Age = 35 });
        people.Add(new Person { FirstName = "Samantha", LastName = "Jones", Occupation = "Dev", Age = 24 });
 
        return people;
    }
}
 
public class Person
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Occupation { get; set; }
    public int Age { get; set; }
}
 
public class FullName
{
    public string First { get; set; }
    public string Last { get; set; }
}