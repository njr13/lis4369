# LIS4369

## Nick Richard

### Project Requirements

#### Project Screenshots:

*Screenshot of Tutorial: Introducing LINQ*

![LINQ Screenshot](img/LINQ1.png)

![LINQ Screenshot](img/LINQ2.png)

![LINQ Screenshot](img/LINQ3.png)

#### Project Links:

*This assignment*

[Project 2 Link](https://bitbucket.org/njr13/lis4369/ "Project 2")