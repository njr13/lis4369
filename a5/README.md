# LIS4369

## Nick Richard

### Assignment Requirements

#### Assignment Screenshots:

*Screenshot of Home Page*

![Home Page Screenshot](img/Home.png)

*Screenshot of About Page*

![About Page Screenshot](img/About.png)

*Screenshot of Contact Page*

![Contact Page Screenshot](img/Contact.png)

#### Assignment Link

[Assignment 5 Link](https://bitbucket.org/njr13/lis4369/ "Assignment 5")