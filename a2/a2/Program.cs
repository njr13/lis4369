﻿using System;

namespace a2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("/////////////////////////////////////////////////");
            Console.WriteLine("A2: Simple Calculator");
            Console.WriteLine("Note: Program does not perform data validation.");
            Console.WriteLine("Author: Nick Richard");
            DateTime thisDay = DateTime.Now;
            Console.WriteLine("Now: " + thisDay.ToString("ddd, MM/dd/yy hh:mm:sst"));
            Console.WriteLine("/////////////////////////////////////////////////\n");
        
        //Initialize Variables
            string num1, num2, oper;
            double n1, n2, oper1, calc;
        
        //Prompt user to imput two numbers
            Console.Write("num1: ");
            num1 = Console.ReadLine();
            n1 = Convert.ToDouble(num1);

            Console.Write("num2: ");
            num2 = Console.ReadLine();
            n2 = Convert.ToDouble(num2);

        //Display possible calculator operations
            Console.WriteLine("\n1 - Addition");
            Console.WriteLine("2 - Subtraction");
            Console.WriteLine("3 - Multiplication");
            Console.WriteLine("4 - Division");

        //Prompt the user to choose a operation
            Console.Write("\nChoose a mathematical operation: ");
            oper = Console.ReadLine();
            oper1 = Convert.ToDouble(oper);
            
        //If ststement that performs operation based on user imput
          //Addition Operation 
            if (oper1 == 1)
            {
                calc = n1+n2;
                Console.WriteLine("\n\n*** Result of addition operation: ***");
                Console.WriteLine(calc);
            }

          //Subtraction Operation
            else if (oper1 == 2)
            {
                calc = n1-n2;
                Console.WriteLine("\n\n*** Result of subraction operation: ***");
                Console.WriteLine(calc);
            }

          //Multiplication operation
            else if (oper1 == 3)
            {
                calc = n1*n2;
                Console.WriteLine("\n\n*** Result of multiplication operation: ***");
                Console.WriteLine(calc);
            }

          //Division operation
            else if (oper1 == 4)
            {

            //While loop to prevent division by zero
                while (n2 == 0)
                {
                    Console.WriteLine("\nCannot divide by zero!");
                    Console.Write("num2: ");
                    num2 = Console.ReadLine();
                    n2 = Convert.ToDouble(num2);
                }
                    calc = n1/n2;
                    Console.WriteLine("\n\n*** Result of division operation: ***");
                    Console.WriteLine(calc);
            }

          //Invalid operation message
            else
            {
                Console.WriteLine("\nAn incorrect mathematical operation was entered!");
            }
        //Allow user to exit program
            Console.Write("\nPress any key to exit!");
            Console.ReadKey();
            Console.WriteLine("\n");
        }
    }
}