# LIS4369

## Nick Richard

### Assignment Requirements

#### Assignment Screenshots:

*Screenshot of Valid Operation*

![Valid Operation Screenshot](img/validOperation.png)

*Screenshot of Invalid Operation*

![Invalid Operation Screenshot](img/invalidOperation.png)

*Screenshot of Extra Credit*

![Extra Credit Screenshot](img/extraCredit.png)

#### Assignment Link

[Assignment 2 Link](https://bitbucket.org/njr13/lis4369/ "Assignment 2")