# LIS4369

## Nick Richard

### Assignment Requirements

#### Git Commands w/short description:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git rm - Remove files from the working tree and from the index

#### Assignment Screenshots:

*Screenshot of hwapp application running*

![hwapp Screenshot](img/Hwapp.png)

*Screenshot of aspnetcoreapp application running*

![aspnetcoreapp Screenshot](img/aspnetcoreapp1.png)

![aspnetcoreapp Screenshot](img/aspnetcoreapp2.png)

[aspnetcoreapp](http://localhost:5000/about "aspnetcoreapp")

#### Assignment Links:

*This assignment*

[Assignment 1 Link](https://bitbucket.org/njr13/lis4369/ "Assignment 1")

*Tutorials*

[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/njr13/bitbucketstationlocations/ "Bitbuucket Station Locations")

[A1 My Team Quotes Tutorial Link](https://bitbucket.org/njr13/myteamquotes/ "My Team Quotes Tutorial")