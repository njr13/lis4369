﻿using System;

namespace a3
{
    class Program
    {
        public static decimal FutureValue(decimal presentValue, int numYears, decimal yearlyInt, decimal monthlyDep)
        {
            int numMonths = numYears * 12;
            double monthlyInt = (double)yearlyInt / 12 / 100;

            return presentValue * (decimal)Math.Pow((1 + monthlyInt), numMonths) + (monthlyDep * (decimal)(Math.Pow((1 + monthlyInt), numMonths) - 1) / (decimal)monthlyInt);
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("//////////////////////////////////////////////////");
            Console.WriteLine("Program Requirements:");
            Console.WriteLine("A3 - Future Value Calculator");
            Console.WriteLine("Author: Nick Richard");
            Console.WriteLine("     1) Use Intrinsic method to display date/time;");
            Console.WriteLine("     2) Research: what is future value? And. its formula;");
            Console.WriteLine("     3) Create FutureValue method using the following parameters: decimal presentvalue, int numYears, decimal yearlyInt, decimal monthlyDep;");
            Console.WriteLine("     4) Initialize suitable variables(s): use decimal data types for currency variables;");
            Console.WriteLine("     5) Perform data validation: prompt user until corract data is entered:");
            Console.WriteLine("     6) Display money in currency format;");
            Console.WriteLine("     7) Allow user to press any key to return back to command line.");
            Console.WriteLine("//////////////////////////////////////////////////");
           
           //Display Date
            Console.WriteLine("\nNow: " + DateTime.Now.ToString("ddd, MM/dd/yy hh:mm:ss t"));
           
           //Initialize Variables
            decimal fv = 0.0m;
            decimal startingBalance = 0.0m;
            int term = 0;
            decimal interestRate = 0.0m;
            decimal monthlyDeposit = 0.0m;
        
      
           //Prompt for Starting Balance
            Console.Write("\nStarting Balance: ");
            while (!Decimal.TryParse(Console.ReadLine(), out startingBalance))
                {
                    Console.WriteLine("Starting balance must be numeric.");
                    Console.Write("Starting Balance: ");
                }
                
           //Prompt user for Term
            Console.Write("\nTerm (years): ");
            while (!int.TryParse(Console.ReadLine(), out term))
                {
                    Console.WriteLine("Term must be integer data type.");
                    Console.Write("Term (years): ");
                }           

           //Prompt User for Interest Rate
            Console.Write("\nInterest Rate: ");
            while (!Decimal.TryParse(Console.ReadLine(), out interestRate))
                {
                    Console.WriteLine("Interest rate must be nueric.");
                    Console.Write("Interest Rate: ");
                }
                
           //Prompt User for Monthly Deposit amount
            Console.Write("\nDeposit (monthly): ");
            while (!Decimal.TryParse(Console.ReadLine(), out monthlyDeposit))
                {
                    Console.WriteLine("Monthly deposit rate must be numeric.");
                    Console.Write("Deposit (monthly): ");
                }

            fv = FutureValue(startingBalance, term, interestRate, monthlyDeposit);  

           //Display Future Value
            Console.WriteLine("\n*** Future Value: ***");
            Console.WriteLine(fv.ToString("C2"));

           //Allow user to exit program
            Console.Write("\nPress any key to exit!");
            Console.ReadKey();
            Console.WriteLine();
          
        }
    }
}