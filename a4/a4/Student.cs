﻿using System;

namespace a2
{
    public class Student : Person
    {

        private string college;
        private string major;
        private double gpa;


        public Student()
        {
            college = "CCI";
            major = "IT";
            gpa = 4.0;

            Console.WriteLine("Creating derived student object from default constructor (accepts no arguments):");
        
        }

        public Student(string fn="", string ln="", int a=0, string c="", string m="", double g=0.0) : base(fn,ln,a)
        {
            college = c;
            major = m;
            gpa = g;

            Console.WriteLine("Creating derived student object from parameterized constructor (accepts arguments)");
        }

        public string GetName()
        {
            return fname + " " + lname;
        }

        public string GetFullName()
        {
            return GetFname() + " " + GetLname();
        }

        public override string GetObjectInfo()
        {
            return base.GetObjectInfo() + " in the college of " + this.college + ", majoring in " + this.major + ", and has a " + this.gpa + " gpa.";
        }
    }

}

