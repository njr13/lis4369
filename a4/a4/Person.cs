﻿using System;

namespace a2
{
    public class Person
    {
        protected string fname;
        protected string lname;
        protected int age; 
        


        public Person()
        {

            fname = "First name";
            lname = "Last name";
            age = 0;

            Console.WriteLine("\nCreating base person object from default contructor (accepts no arguments):");

            Console.WriteLine("\nCreating " + this.fname + " " + this.lname + " person object from default constructor (accepts no arguments):");

        }

        public Person( string fn ="", string ln ="" , int a=0) 
        {

            fname = fn;
            lname = ln;
            age = a;

            Console.WriteLine("Creating base person object from parameterized constructor (accepts arguments):");

            Console.WriteLine("Creating " + this.fname + " " + this.lname + " person base object from parameterized constructor (accepts arguments):");

        }

        //**Mutator Methods
        //setter methods */

        public void SetFname(string fn="")
        {
            fname = fn;
        }

        public void SetLname(string ln="")
        {
            lname = ln;
        }

        public void SetAge(int a=0)
        {
            age = a;
        }

        //**accessor methods** */
        public string GetFname()
        {
            return fname;
        }

        public string GetLname()
        {
            return lname;
        }

        public int GetAge()
        {
            return age;
        }

        public virtual string GetObjectInfo()
        {
            return fname + " " + lname + " " + " is " + age.ToString();
        }
    }
}