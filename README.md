# LIS4369 - Extensible Enterprise Solutions

## Nick Richard

### Assignment Requirements:

*Course Work Links:*

1 [A1 README.md](a1/README.md)

    - install .NET Core
    - Create hwapp application
    - Create aspnetcoreapp application
    - Provide screenshots of instalations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials 
    - Provide git command descriptions

2 [A2 README.md](a2/README.md)

    - Display short assignment requirements
    - Display *your* name as “author”
    - Display current date/time
    - Must perform and display each mathematical operation

3 [A3 README.md](a3/README.md)

    - Display short assignment requirements
    - Display name as "author"
    - Display current date/time
    - Create Future Value Calculator

4 [A4 README.md](a4/README.md)

    - Display short assignment requirements.
    - Display *your* name as “author.”
    - Display current date/time (must include date/time, your format preference).
    - Create two classes: person and student (see fields and methods below).
    - Must include data validation on numeric data.

5 [A5 README.md](a5/README.md)

    - Modify carousel images and link to promotional sites
    - Modify subheadings to promote you
    - Modify About page to display name and current time on server
    - Modify contact page
    - Create a favicon.ico using your initials
    - Modify footer to display your name

6 [P1 README.md](p1/README.md)

    - Create Room class
    - Create fields (properties or data members)
    - Create constructors
    - Create mutator methods
    - Create accessor methods
    - Provide screenshot of calculator

7 [P2 README.md](p2/README.md)

    - Prompt the user for last name. Return full name, occupation, and age
    - Prompt user for age and occupation (Dev or Manager). Return full name
    - Allow user to press any key to return back to command line
